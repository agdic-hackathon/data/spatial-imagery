# Spatial imagery

## Selected datasets

| Name | Description | Source | Type | Format | License | Last updated |
| ---- | ----------- | ------ | ---- | ------ | ------- | ------------ |
| [SPOT](/SPOT) | Cover from<br>- SPOT6/7 (1.5m)<br>- SPOT6/7 (6m)<br>- SPOT5 (10m) | Airbus DS Geo / IGN-FI | Vector | Shapefile | Unknown  | Unknown  |

Because the actual SPOT imagery datasets are very heavy and subject to a specific license, they're provided on hard drives and available on site. After you've reviewed the different SPOT covers from the shapefiles and found what you need, please request the imagery datasets from the hackathon organizers.

[Other available datasets are documented here.](https://framagit.org/agdic-hackathon/data/spatial-imagery/wikis/home)


**All spatial imagery will be made available on site. Speak to the organizers for more information.**
